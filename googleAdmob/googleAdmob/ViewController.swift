//
//  ViewController.swift
//  googleAdmob
//
//  Created by ruben munoz on 22/03/2020.
//  Copyright © 2020 ruben munoz. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ViewController: UIViewController, GADInterstitialDelegate, GADRewardedAdDelegate {
    
    
    
    var interstitial: GADInterstitial!
    var rewarded: GADRewardedAd?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/5135589807")
        interstitial.delegate = self
        interstitial = loadInterstitial()
        rewarded = createAndLoadRewardedAd()
        
    }
    
    
    func loadInterstitial() -> GADInterstitial {
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = loadInterstitial()
    }
    
    func createAndLoadRewardedAd() -> GADRewardedAd {
        rewarded = GADRewardedAd(adUnitID: "ca-app-pub-3940256099942544/1712485313")
        rewarded?.load(GADRequest()) { error in
            if let error = error {
                print("Loading failed: \(error)")
            } else {
                print("Loading Succeeded")
            }
        }
        return rewarded!
    }
    
    func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
        rewarded = createAndLoadRewardedAd()
    }
    func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
    }
    
    @IBAction func showBanner(_ sender: Any) {
        
    }
    
    //show the ad
    @IBAction func showInterstitial(_ sender: Any) {
        if interstitial.isReady {
            interstitial.load(GADRequest())
            interstitial.present(fromRootViewController: self)
        }
    }
    
    //show the ad
    @IBAction func showRewarded(_ sender: Any) {
        if rewarded?.isReady == true {
            rewarded!.present(fromRootViewController: self, delegate:self)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.destination is DetailViewController else { return }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

