Screenshoots
============

![alt text](https://gitlab.com/ruben_munoz/googleadmob/uploads/216de60df4f8d803f44d5dcca5c9c0d5/Screenshot_2020-03-26_at_17.06.51.png)
![alt text](https://gitlab.com/ruben_munoz/googleadmob/uploads/dd2c1102e66c8e5c97687deb3f99bace/Screenshot_2020-03-26_at_17.07.12.png)
![alt text](https://gitlab.com/ruben_munoz/googleadmob/uploads/aff268c840b4a6bf9e991913942fe443/Screenshot_2020-03-26_at_17.08.14.png)
![alt text](https://gitlab.com/ruben_munoz/googleadmob/uploads/0d7247b9682ecf3d35fd92ec3d7c1b63/Screenshot_2020-03-26_at_17.07.24.png)

About
-----

Is a UIViewController used to display google banner ads, interstitial ads, and bonus video ads.

Requirements
------------


*  Swift 5

*  Xcode 11

Installation
------------


1. Import the Mobile Ads SDK 


*  Install CocoaPods (sudo gem install cocoapods)
*  Create a Podfile (pod init)
*  Open your Podfile (nano podfile)
*  Add a CocoaPod after putting up the use_frameworks! (pod 'Google-Mobile-Ads-SDK')
*  Run install to integrate CocoaPods (pod install --repo-update)
*  To avoid failure when compiling the sdk (pod install --verbose)

2.  Update your Info.plist


*  In your app's Info.plist file, add a GADApplicationIdentifier key with a string value of your AdMob app ID 

    Source code (<key>GADApplicationIdentifier</key> <string>ca-app-pub-3489093835292178~6887301116</string>)

![alt text](https://gitlab.com/ruben_munoz/googleadmob/uploads/478b67bf6bc5acf46282179f97e7109f/Screenshot_2020-03-26_at_19.16.40.png)

*  Add the NSAllowsArbitraryLoads, NSAllowsArbitraryLoadsForMedia, and NSAllowsArbitraryLoadsInWebContent exceptions to your app's Info.plist file to disable ATS restrictions.
   
![alt text](https://gitlab.com/ruben_munoz/googleadmob/uploads/2e3e0a87b9cf55b58c06b3c404256593/Screenshot_2020-03-26_at_19.16.53.png)

3.  Initialize mobile ads in your AppDelegate

    Before loading ads, call the startWithCompletionHandler: method on the GADMobileAds.sharedInstance, which initializes the SDK 

![alt text](https://gitlab.com/ruben_munoz/googleadmob/uploads/7886ae651780f175c3545f11f41fe781/Screenshot_2020-03-27_at_00.23.53.png)
